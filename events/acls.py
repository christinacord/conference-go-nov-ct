from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY

import requests
import json

# create 3 functions, one for each api


def get_city_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    params = {
        "per_page": 1,
        "query": city + " " + state,
    }
    url = "https://api.pexels.com/v1/search"
    response = requests.get(url, params=params, headers=headers)
    content = json.loads(response.content)
    try:
        return {"picture_url": content["photos"][0]["src"]["original"]}
    except (KeyError, IndexError):
        return {"picture_url": None}


def get_weather_data():
    # Create the URL for the geocoding API with the city and state
    url = ""
    # Make the request
    response = requests.get(url)
    # Parse the JSON response
    content = json.loads(response.content)
    # Get the latitude and longitude from the response
    return {}

    # Create the URL for the current weather API with the latitude
    #   and longitude
    # Make the request
    # Parse the JSON response
    # Get the main temperature and the weather's description and put
    #   them in a dictionary
    # Return the dictionary
    pass
