from common.json import ModelEncoder
from .models import Conference


class ConferenceListEncoder(ModelEncoder):
    model = Conference
    properties = []
